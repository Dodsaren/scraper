const cheerio = require('cheerio')
const {
  request,
  isObjLiteral
} = require('./utils.js')

class Scraper {
  get (url, schema = null) {
    return request(url)
      .catch(err => {
        console.log('Unhandled error', err.statusCode)
      })
      .then(body => {
        const $ = cheerio.load(body, { decodeEntities: false })
        if (!schema) {
          return $
        }
        return {
          url,
          og: this.og($),
          ...this.iterate($, schema)
        }
      })
  }
  og ($) {
    const og = {}
    $('meta[property^="og:"]').each((i, el) => {
      const key = $(el).attr('property').split('og:')[1]
      const value = $(el).attr('content')
      return og[key] = value
    })
    return og
  }
  iterate ($, schema, context = null) {
    return Object.entries(schema).reduce((o, i) => {
      const [key, value] = i
      o[key] = value
      if (isObjLiteral(value)) {
        o[key] = this.iterate($, schema[key])
      }
      else if (Array.isArray(value)) {
        const [selector, obj] = value
        o[key] = $(selector).map((i, elem) => {
          if (!obj) {
            return this.parse(elem, $)
          }
          return this.iterate($, obj, elem)
        }).get()
      }
      else {
        o[key] = this.parse(value, $, context)
      }
      return o
    }, {})
  }
  parse (target, $, context = null) {
    if (typeof target === 'string') {
      return $(target, context).text().replace(/\s\s+/g, '')
    }
    if (typeof target === 'function') {
      return target($, context)
    }
    return $(target).text().replace(/\s\s+/g, '')
  }
}

module.exports = new Scraper()
