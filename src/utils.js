module.exports = {
  request: (url) => {
    return new Promise((resolve, reject) => {
    const lib = url.startsWith('https') ? require('https') : require('http')
    const request = lib.get(url, (response) => {
      if (response.statusCode < 200 || response.statusCode > 299) {
        reject(response)
      }
      const body = []
      response.on('data', chunk => body.push(chunk))
      response.on('end', () => resolve(body.join('')))
    })
    request.on('error', (err) => reject(err))
    })
  },
  isObjLiteral: (_obj) => {
    var _test  = _obj
    return (typeof _obj !== 'object' || _obj === null ?
      false : ((function () {
        while (!false) {
          if (  Object.getPrototypeOf( _test = Object.getPrototypeOf(_test)  ) === null) {
            break
          }
        }
        return Object.getPrototypeOf(_obj) === _test
      })())
    )
  }
}
